require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = User.new(name:"Julio", email:"dicafis@gmail.com")
  end

  test "should be valid" do
    assert @user.valid?
  end

  test "name should be present" do
    @user.name = "     "
    assert_not @user.valid?
  end

  test "email should be present" do
    @user.email = ""
    assert_not @user.valid?
  end

  test "name should not be too long" do
    @user.name = "Julio" * 11
    assert_not @user.valid?
  end

  test "email should not be too long" do
    @user.email = "@" * 101
    assert_not @user.valid?
  end

  test "email validation should accept valid adresses" do
    valid_adresses = %w[user@example.com USER@foo.com A_US_ER@foo.bar.org]
    valid_adresses.each do |adress|
      @user.email = adress
      assert @user.valid?
    end
  end

  test "email validation should reject invalid adresses" do
    invalid_adresses = %w[user@example,com user_at_foo.org name@example@bar.com]
    invalid_adresses.each do |adress|
      @user.email = adress
      assert_not @user.valid?
    end
  end

  test "password must be equal" do
    @user = User.new(name: "Mary", email:"maricela.valdez@thincode.com", password: "chocolatito", password_confirmation:"chocolatito")
    assert @user.valid?
  end
end
