class UsersController < ApplicationController
  # before_action se ejecutará antes de realizar cualquier acción.
  # only: este before_action solo se ejecutará para las acciones edit, update y show
  before_action :loggued_in_user, only: [:edit, :update, :show]
  # este before_action verificará que además sea el propio usuario y no quiera hacer edit o update para un usuario diferente
  before_action :correct_user, only: [:edit, :update]

  def show
    @user = User.find(params[:id])
    @micropost = @user.microposts.paginate(page: params[:page])
  end

  def new
    # Como estamos creando un nuevo usuario, los campos en la forma NEW aparecerán en blanco
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "Welcome! Now you exist in Micropost community"
      redirect_to @user # Se transforma en: render <url> | render users_url(@user)
    else
      render 'new'
    end
  end

  def edit
    # Como estamos yendo por un usuario a la BD, los campos de la forma EDIT aparecerán llenos
    @user = User.find_by(id: params[:id]) # si find no encuentra registro, levanta una excepción. find_by regresa false en caso de que no encuentre registro por lo que resulta más controlable
  end

  def update
    @user = User.find_by(id: params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Data updated!"
      redirect_to @user
    else
      flash[:danger] = "Data couldn't be updated, please retry"
      render 'edit'
    end
  end

  def index
#    @users = User.all
    @users = User.paginate(page: params[:page], :per_page => 20)
  end

  private
    def user_params # Para no actualizar todo el objeto de user (por convención no se permite porque podrías inyectar código/datos maliciosos)
                    # creamos user_params para que solo sean válidos, cierto número de campos/hashes para actualizar en BD
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end

# Se cambia hacia application_controller porque también la necesitará microposts_controller
#    def loggued_in_user
#      unless logged_in?
#        flash[:danger] = "Please login before you can do this!"
#        redirect_to login_url
#      end
#    end

    def correct_user
      @user = User.find_by(id: params[:id])
      unless current_user?(@user)
        flash[:danger] = "Have no privileges to do that!"
        redirect_to(root_url)  # app > helpers > sessions
      end
    end
end
