class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    puts user.inspect
    puts params.inspect
    if user && user.authenticate(params[:session][:password])
      log_in(user)
      redirect_to user
    else
      flash.now[:danger] = "Your data doesn't match any valid user"
      render 'new' # Si falla, mandará la acción new que viene al controlador por el "def new"
    end
  end

  def destroy
    log_out
    redirect_to root_path
  end
end
