class MicropostsController < ApplicationController
  before_action :loggued_in_user, only: [:create, :destroy]
  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = "MicroPost created!"
      redirect_to root_url
    else
      render 'static_pages/home'
    end
  end

  private
    def micropost_params
      params.require(:micropost).permit(:content)
    end
end
