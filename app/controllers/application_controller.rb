class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  # Helper de sesión
  include SessionsHelper

  private
    def loggued_in_user
      unless logged_in?
        flash[:danger] = "Please login before you can do this!"
        redirect_to login_url
      end
    end
end
