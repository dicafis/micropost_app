class StaticPagesController < ApplicationController
  def home
    if logged_in?
      @micropost = current_user.microposts.build
      @timeline_items = current_user.timeline.paginate(page: params[:page])
    end
  end

  def help
  end

  def about
  end

  def contact
  end
end
