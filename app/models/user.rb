class User < ApplicationRecord
  regexp = /\A[\w+\-._]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :name, presence: true,
            length: { maximum: 50 }
  validates :email, presence: true,
            length: { maximum: 100 },
            format: { with: regexp},
            uniqueness: {case_sensitive: false}
  has_secure_password
  validates :password, presence:true, length:{minimum:6}, allow_nil:true
  has_many :microposts, dependent: :destroy #Un user tiene muchos micropost y si elimino al usuario, entonces borrará todos los micropost ligados
  def timeline
    Micropost.where("user_id = ?", id)
  end
end
