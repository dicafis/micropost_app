Rails.application.routes.draw do
  get 'sessions/new'

#  get 'users/new'

#  get 'static_pages/home'

#  get 'static_pages/help'

#  get 'static_pages/contact'

  root 'static_pages#home'

  get '/home', to:'static_pages#home'
  get '/about', to:'static_pages#about'
  get '/help' , to:'static_pages#help'
  get '/contact', to:'static_pages#contact'
  get '/signup', to:"users#new"
  resources :users # resources crea POST, DELETE, GET, EDIT... (todo el rest)
  get    '/login' , to:'sessions#new'
  post   '/login' , to:'sessions#create'
  delete '/logout', to:'sessions#destroy'

  resources :microposts, only: [:create, :destroy]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
